﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.DataLayer.Models;
namespace WPFFirstTaskV2.DataLayer.Repositories
{
    public class TaskRepository : Repository<Models.TaskModel>
    {
        public TaskRepository(AppContext procCont) : base(procCont)
        {

        }

        /// <summary>
        /// Метод получения последнего элемента из БД
        /// </summary>
        /// <returns> Last Element</returns>
        public override async Task<TaskModel> GetLastAsync()
        {
             return await Task.Run(() => ProcessContext.TaskModels.ToList().Last());
        }

        public override async Task UpdateAsync(TaskModel item)
        {

            var a = await ProcessContext.TaskModels.FindAsync(item.Id);
            a.Name = item.Name;
            a.ProcessId = item.ProcessId;
            a.СompletionDate = item.СompletionDate;
            a.CreatingDate = item.CreatingDate;
            await ProcessContext.SaveChangesAsync();
        }
    }
}
