﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.BusinessLayer.ModelsDTO;
using WPFFirstTaskV2.BusinessLayer.Services;
using WPFFirstTaskV2.DataLayer.Models;
using WPFFirstTaskV2.DataLayer.Repositories;

namespace WPFFirstTaskV2.BusinessLayer.User
{
    /// <summary>
    /// логика для авторизации аккаунта
    /// </summary>
    public class Authorization
    {
        /// <summary>
        /// получает аккаунт по логину и паролю
        /// </summary>
        /// <param name="_login">логин</param>
        /// <param name="password">пароль</param>
        /// <returns> аккаунт</returns>
        public async Task<AccountDTO> GetAccount(string _login, string password)
        {
            UserRepository userRepository = new UserRepository(new AppContext());

            var account = await userRepository.FindAsync(_login);

            if (account == null)
            {
                throw new NotImplementedException();
            }

            if (!account.Password.Equals(password))
            {
                throw new NotImplementedException();
            }

            var processes = new ProcessesService();

            var accProcess = processes.GetAll().Where(t => t.UserId == account.Id);

            var tasks = new TasksServices();
            var processesTasks = tasks.GetAll();

            foreach (var a in accProcess)
            {
                try
                {
                    a.Tasks = processesTasks.Where(t => t.ProcessId == a.Id);
                }
                catch { }
                
            }

            return new AccountDTO()
            {
                Id = account.Id,
                AccountName = account.Login,
                AccountPassword = account.Password,
                AccountProcesses = accProcess
            };
        }


    }
    
}
