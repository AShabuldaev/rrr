﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfFirstTask.V2.ProgramEventArgs;
using WPFFirstTaskV2.BusinessLayer.User;

namespace WpfFirstTask.V2.Code
{
    /// <summary>
    /// Обрабатывает запросы пользователя при авторизации
    /// </summary>
    class RegistrationController
    {
        /// <summary>
        /// Производит операцию регистрации пользователя, 
        /// 
        /// </summary>
        /// <param name="window">рабочее окно приложения</param>
        /// <returns></returns>
        public async Task Registrate(MainWindow window)
        {
            if (window.LoginBoxRegistr.Text == string.Empty || window.PasswordBoxRegistr.Password == string.Empty)
            {
                System.Windows.MessageBox.Show(window.FindResource("EmptyLogginOrPasswErr").ToString());

                return;
            }
            if (!window.PasswordBoxRegistr.Password.Equals(window.ConfirmPasswordBox.Password))
            {
                
                System.Windows.MessageBox.Show(window.FindResource("PasswordsNotEquals").ToString());
                return;
            }

            Registration registration = new Registration();
            Regist += Registrating;
            UserRegistrated += RegistratingOK;
            if (await registration.IsEnableLogin(window.LoginBoxRegistr.Text))
            {
                Regist?.Invoke(window, new ProgramStateEventArgs(window.FindResource("Registrating").ToString()));

                registration.CreateNewAccount(window.LoginBoxRegistr.Text, window.ConfirmPasswordBox.Password);

                window.LoginBoxRegistr.Text = string.Empty;
                window.ConfirmPasswordBox.Password = string.Empty;
                window.LoginBoxRegistr.Text = string.Empty;
                
            }
            else
            {
                System.Windows.MessageBox.Show(window.FindResource("ThisNameExistYet").ToString());
                throw new NotImplementedException();
            }
            UserRegistrated?.Invoke(window, new ProgramStateEventArgs(window.FindResource("RegistratingSuccsecful").ToString()));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">вызывающий объект(MainWindow)</param>
        /// <param name="e">аргументы (выводимое сообщение)</param>
        delegate void RegistratingStateHandler(object sender, ProgramStateEventArgs e);
        /// <summary>
        /// ивент при регистрации(выводит сообщение о том,что происходит регистрация)
        /// </summary>
        event RegistratingStateHandler Regist;
        /// <summary>
        /// ивент при успешной регистрации
        /// </summary>
        event RegistratingStateHandler UserRegistrated;
        /// <summary>
        /// Передает в строку состояния информацию о том,что происходит регистрация
        /// </summary>
        /// <param name="sender">вызывающий объект(MainWindow)</param>
        /// <param name="e"> аргументы (выводимое сообщение) </param>
        private void Registrating(object sender, ProgramStateEventArgs e)
        {
            MainWindow window = sender as MainWindow;

            window.StateTextBlock.Text = e.Message;
        }
        /// <summary>
        /// Передает в строку состояния информацию о том,что была выполнена регистрация
        /// </summary>
        /// <param name="sender">вызывающий объект(MainWindow)</param>
        /// <param name="e">аргументы (выводимое сообщение) </param>
        private async void RegistratingOK(object sender, ProgramStateEventArgs e)
        {
            MainWindow window = sender as MainWindow;

            window.StateTextBlock.Text = e.Message;
            await Task.Run(async () =>
            {
                await Task.Delay(window.DelayStateString);

            });

            window.StateTextBlock.Text = string.Empty;
        }
    }
}
