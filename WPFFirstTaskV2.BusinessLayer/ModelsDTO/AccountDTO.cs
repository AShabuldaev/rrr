﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFFirstTaskV2.BusinessLayer.ModelsDTO
{
    /// <summary>
    /// промежуточная модель аккаунта
    /// </summary>
    public class AccountDTO
    {
       
        public int Id { get; set; }
        public IEnumerable<ProcessDTO> AccountProcesses { get; set; }
        public string AccountName { get; internal set; }
        public string AccountPassword { get; set; }
    }
}
