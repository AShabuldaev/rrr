﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.DataLayer.Models;
using WPFFirstTaskV2.DataLayer.Repositories;

namespace WPFFirstTaskV2.BusinessLayer.User
{
    public class Registration
    {
        /// <summary>
        /// создание нового аккаунта пользователя
        /// </summary>
        /// <param name="login"> придуманный логин</param>
        /// <param name="password"> придуманный пароль</param>
        public void CreateNewAccount(string login, string password)
        {
            UserModel user = new UserModel()
            {
                Login = login,
                Password = password
            };

            UserRepository userRepository = new UserRepository(new AppContext());

             userRepository.CreateAsync(user);
        }
        /// <summary>
        /// проверка существования  логина(имени аккаунта)
        /// </summary>
        /// <param name="login"> придуманный логин</param>
        /// <returns>true если он существует, false,если нет </returns>
        /// 
        public async Task<bool> IsEnableLogin(string login)
        {
            UserRepository userRepository = new UserRepository(new AppContext());

            var findedLog = await userRepository.FindAsync(login);

            if (findedLog == null)
                return true;

            return false;
        }
    }
}
