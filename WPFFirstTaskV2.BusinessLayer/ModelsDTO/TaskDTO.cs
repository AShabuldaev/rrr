﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFFirstTaskV2.BusinessLayer.ModelsDTO
{
    /// <summary>
    /// промежуточная модель задачи
    /// </summary>
    public class TaskDTO
    {
        
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatingDate { get; set; }
        public DateTime СompletionDate { get; set; }
        public int ProcessId { get; set; }
    }
}
