﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WPFFirstTaskV2.BusinessLayer.ModelsDTO;
using WPFFirstTaskV2.BusinessLayer.Services;
using System.Collections.ObjectModel;
namespace WpfFirstTask.V2.ViewModels
{
    public class ProcessViewModel : INotifyPropertyChanged
    {

        /// <summary>
        /// описывает модель представления процесса
        /// </summary>
        private ProcessDTO ProcessDTO;

        public ProcessViewModel(ProcessDTO process)
        {
            ProcessDTO = process;
        }


        ProcessesService processes = new ProcessesService();

        public ProcessViewModel()
        {

        }

        public ObservableCollection<TaskViewModel> Tasks { get; set; } = new ObservableCollection<TaskViewModel>();

        public int Id
        {
            get
            {
                return ProcessDTO.Id;
            }
            set
            {
                ProcessDTO.Id = value;
                OnPropertyChanged(nameof(Id));
            }
        }
        public string Name
        {
            get
            {
                return ProcessDTO.Name;
            }
            set
            {
                ProcessDTO.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public DateTime CreatingDate
        {
            get
            {
                return ProcessDTO.CreatingDate;
            }
            set
            {
                ProcessDTO.CreatingDate = value;
                OnPropertyChanged(nameof(CreatingDate));
            }
        }

        public DateTime СompletionDate
        {
            get
            {
                return ProcessDTO.СompletionDate;
            }
            set
            {
                ProcessDTO.СompletionDate = value;
                OnPropertyChanged(nameof(СompletionDate));
            }
        }

        public int UserId
        {
            get
            {
                return ProcessDTO.UserId;
            }
            set
            {
                ProcessDTO.UserId = value;
                OnPropertyChanged(nameof(UserId));
            }
        }
        //public void GetData(ProcessDTO item)
        //{
        //    ProcessDTO = item;
        //}

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


    }
}
