﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.BusinessLayer.Services;
using WPFFirstTaskV2.BusinessLayer.ModelsDTO;
using WpfFirstTask.V2.ViewModels;
namespace WpfFirstTask.V2.Code
{
    /// <summary>
    /// Обрабатывает запросы пользователя при работе с процессами
    /// </summary>
    public class ProcessController
    {
        /// <summary>
        /// Обрабатывает запрос на создание нового процесса
        /// </summary>
        /// <param name="window">рабочее окно приложения</param>
        public async void AddNewProcess(MainWindow window)
        {
            ProcessesService processesService = new ProcessesService();

            ProcessDTO processDTO = new ProcessDTO()
            {
                Name = window.NameTxtBCreatingTab.Text,
                СompletionDate = (DateTime)window.СompletionDateCreatingTab.SelectedDate,
                CreatingDate = (DateTime)window.CreatingDateCreatingTab.SelectedDate,
                UserId = window.CurrentAccount.Id
            };
            //добавить в бд процесс
            await processesService.Add(processDTO);
            //получение последнего элемента (созданного аккаунта)
            processDTO = await processesService.LastAsync();
            //изменение строки состояния
            window.CurrentAccount.AccountProcesses.Add(new ProcessViewModel(processDTO));
            window.NameTxtBCreatingTab.Text = string.Empty;
            window.СompletionDateCreatingTab.SelectedDate = null;
            window.CreatingDateCreatingTab.SelectedDate = null;
        }
        /// <summary>
        /// Обрабатывает запрос на удаление процесса
        /// </summary>
        /// <param name="account">аккаунт в котором следует удалить процесс</param>
        /// <param name="process"> удаляемый процесс</param>
        public async void DeleteProcess(AccountViewModel account,ProcessViewModel process)
        {
            ProcessesService processesService = new ProcessesService();

            await processesService.Delete(process.Id);

            account.AccountProcesses.Remove(process);
            processesService.RemoveTasks(process.Id);
        }
        /// <summary>
        /// Обрабатывает запрос на изменение процесса
        /// </summary>
        /// <param name="processView">изменяемый процесс</param>
        public async void Edit(ProcessViewModel processView)
        {
            ProcessesService processesService = new ProcessesService();

            await processesService.Edit(new ProcessDTO()
            {
                Id = processView.Id,
                CreatingDate = processView.CreatingDate,
                Name = processView.Name,
                СompletionDate = processView.СompletionDate,
                UserId = processView.UserId
            });
        }
    }
}
