﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.DataLayer.Interfaces;
using WPFFirstTaskV2.DataLayer.Models;
namespace WPFFirstTaskV2.DataLayer.Repositories
{
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// Предоставляет методы для работы с пользователями
        /// </summary>
        AppContext ProcessContext;

        /// <summary>
        /// Производит поиск элемента по его логину в бд
        /// </summary>
        /// <param name="login" > Имя аккаунта,который нужно найти</param>
        /// <returns> Возвращает найденный акк, если его нет,то нулл</returns>
        public async Task<UserModel> FindAsync(string login)
        {
           
            UserModel user;
            try
            {
                var db = await Task.Run(() => ProcessContext.UserModels.Where(t => t.Login.Equals(login)));
                user = db.First();
            }
            catch (InvalidOperationException)
            {
                user = null;
            }
            return user;

        }

        public UserRepository(AppContext procCont)
        {
            ProcessContext = procCont;
        }

        //метод для создания нового пользователя
        public async void CreateAsync(UserModel user)
        {
            ProcessContext.UserModels.Add(user);
            await ProcessContext.SaveChangesAsync();
        }
    }
}
