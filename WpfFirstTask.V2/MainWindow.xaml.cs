﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfFirstTask.V2.Code;
using WpfFirstTask.V2.ViewModels;
using WpfFirstTask.V2.ProgramEventArgs;
namespace WpfFirstTask.V2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public int DelayStateString { get; private set; } = 5000;
       
        public MainWindow()
        {
            InitializeComponent();

            App.LanguageChanged += LanguageChanged;
            
            CultureInfo currLang = App.Lang;
            

            LangMenu.Items.Clear();
            foreach (var lang in App.Languages)
            {
                MenuItem menuLang = new MenuItem();
                menuLang.Header = lang.DisplayName;
                menuLang.Tag = lang;
                menuLang.IsChecked = lang.Equals(currLang);
                menuLang.Click += ChangeLanguageClick;
                LangMenu.Items.Add(menuLang);
            }
            
        }

        private void LanguageChanged(object sender, EventArgs e)
        {
            CultureInfo currLang = App.Lang;

            //Отмечаем нужный пункт смены языка как выбранный язык
            foreach (MenuItem a in LangMenu.Items)
            {
                CultureInfo cult = a.Tag as CultureInfo;
                a.IsChecked = cult != null && cult.Equals(currLang);
            }
        }

        private void ChangeLanguageClick(object sender, EventArgs e)
        {
            MenuItem mi = sender as MenuItem;
            if (mi != null)
            {
                CultureInfo lang = mi.Tag as CultureInfo;
                if (lang != null)
                {
                    App.Lang = lang;
                }
            }

        }

        public AccountViewModel CurrentAccount = new AccountViewModel();

        private async void CreateAccountClick(object sender, RoutedEventArgs e)
        {
            StateTextBlock.Text = this.FindResource("Registrating").ToString();
            RegistrationController registrationController = new RegistrationController();

            await registrationController.Registrate(this);

            TabCon.SelectedItem = TabCon.Items[ProcessesTab.TabIndex];

            StateTextBlock.Text = this.FindResource("RegistratingSuccsecful").ToString();
            await Task.Run(() => {
                Task.Delay(DelayStateString);
               
            });
            StateTextBlock.Text = string.Empty;
        }

        private async void AuthorizingClick(object sender, RoutedEventArgs e)
        {
            
            OkAuthorizeBut.IsEnabled = false;
            try
            {
                AutorizingController autorizingController = new AutorizingController();

                CurrentAccount = await autorizingController.EnterAccount(this);

                TabCon.SelectedItem = TabCon.Items[ProcessesTab.TabIndex];
                ProcessList.ItemsSource = CurrentAccount.AccountProcesses;

                
                DelProcBut.IsEnabled = false;
                EditBut.IsEnabled = false;
                ViewTaskBut.IsEnabled = false;
                CurrentAccountName.DataContext = CurrentAccount;

            }
            catch (NotImplementedException)
            {

            }
            catch
            {

            }
            OkAuthorizeBut.IsEnabled = true;
        }

        private void AddNewProcessClick(object sender, RoutedEventArgs e)
        {
            ProcessController processController = new ProcessController();

            processController.AddNewProcess(this);

            TabCon.SelectedItem = TabCon.Items[ProcessesTab.TabIndex];
        }

        private void GoToAddTab(object sender, RoutedEventArgs e)
        {
            TabCon.SelectedItem = TabCon.Items[CreatingTabTasks.TabIndex];
        }

        private void ShowTasksClick(object sender, RoutedEventArgs e)
        {
            CurrentAccount.CurrentProcess = (ProcessViewModel)ProcessList.SelectedItem;
            TasksList.ItemsSource = CurrentAccount.CurrentProcess.Tasks;
            TabCon.SelectedItem = TabCon.Items[TasksTab.TabIndex];
            //Set Binding
        }

        private void AddNewTaskClick(object sender, RoutedEventArgs e)
        {
            TaskController taskController = new TaskController();
            taskController.AddNewTask(this);
            TabCon.SelectedItem = TabCon.Items[TasksTab.TabIndex];
        }

        private void EditEementClick(object sender, RoutedEventArgs e)
        {
            ProcessController processController = new ProcessController();
            processController.Edit((ProcessViewModel)ProcessList.SelectedItem);
            ProcessList.ItemsSource = CurrentAccount.AccountProcesses;
            TabCon.SelectedItem = TabCon.Items[ProcessesTab.TabIndex];
        }

        private void ToEditProcessTabClick(object sender, RoutedEventArgs e)
        {
            TabCon.SelectedItem = TabCon.Items[EditionTab.TabIndex];
           
            EditingProcess.DataContext = (ProcessViewModel)ProcessList.SelectedItem;
           
        }

        private void ToEditTaskTabClick(object sender, RoutedEventArgs e)
        {
            TabCon.SelectedItem = TabCon.Items[EditionTaskTab.TabIndex];

            EditingTask.DataContext = (TaskViewModel)TasksList.SelectedItem;

        }

        private void EditTaskClick(object sender, RoutedEventArgs e)
        {
            TaskController taskController = new TaskController();
            taskController.Edit((TaskViewModel)TasksList.SelectedItem);
            TabCon.SelectedItem = TabCon.Items[TasksTab.TabIndex];
        }

        private void DeleteTaskClick(object sender, RoutedEventArgs e)
        {
            TaskController taskController = new TaskController();
            taskController.DeleteTask(CurrentAccount.CurrentProcess,(TaskViewModel)TasksList.SelectedItem);
            TabCon.SelectedItem = TabCon.Items[TasksTab.TabIndex];
        }

        private void DeleteProcess(object sender, RoutedEventArgs e)
        {
            ProcessController processController = new ProcessController();
            processController.DeleteProcess(CurrentAccount,(ProcessViewModel)ProcessList.SelectedItem);
            TabCon.SelectedItem = TabCon.Items[ProcessesTab.TabIndex];
        }

        private void ToProcessTab(object sender, RoutedEventArgs e)
        {
            TabCon.SelectedItem = TabCon.Items[ProcessesTab.TabIndex];
        }

        private void ToAuthorizationClick(object sender, RoutedEventArgs e)
        {
            TabCon.SelectedItem = TabCon.Items[AuthorizationTab.TabIndex];
        }

        private void ToRegistrationClick(object sender, RoutedEventArgs e)
        {
            TabCon.SelectedItem = TabCon.Items[RegistrationTab.TabIndex];
        }
        
        private void ElementSelected(object sender, SelectionChangedEventArgs e)
        {
            if(ProcessList.SelectedItem != null)
            {
                DelProcBut.IsEnabled = true;
                EditBut.IsEnabled = true;
                ViewTaskBut.IsEnabled = true;
            }
            else
            {
                DelProcBut.IsEnabled = false;
                EditBut.IsEnabled = false;
                ViewTaskBut.IsEnabled = false;
            }
        }

        private void GoToAddProc(object sender, RoutedEventArgs e)
        {
            TabCon.SelectedItem = TabCon.Items[CreatingTab.TabIndex];
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            TabCon.SelectedItem = TabCon.Items[TabCon.SelectedIndex-1];
        }

        private void TaskSelected(object sender, SelectionChangedEventArgs e)
        {
            if(TasksList.SelectedItem != null)
            {
                DeleteTaskBut.IsEnabled = true;
                EditTaskBut.IsEnabled = true;
            }
            else
            {
                DeleteTaskBut.IsEnabled = false;
                EditTaskBut.IsEnabled = false;
            }
        }
    }
}
