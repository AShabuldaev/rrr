﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.DataLayer.Models;

namespace WPFFirstTaskV2.DataLayer.Repositories
{
    public class ProcessRepository : Repository<ProcessModel>
    {
        /// <summary>
        /// репозиторий для процессов
        /// </summary>
        /// <param name="procCont"> Принимает контекст данных</param>
        public ProcessRepository(AppContext procCont) : base(procCont)
        {

        }
        public override async Task<ProcessModel> GetLastAsync()
        {

            return await Task.Run(()=> ProcessContext.ProcessModels.ToList().Last());
        }
        /// <summary>
        /// сохраняет изменения выбранного элемента
        /// </summary>
        /// <param name="item"></param>
        public override async Task UpdateAsync(ProcessModel item)
        {
            var a = await ProcessContext.ProcessModels.FindAsync(item.Id);
            a.Name = item.Name;
            a.UserId = item.UserId;
            a.СompletionDate = item.СompletionDate;
            a.CreatingDate = item.CreatingDate;
            await ProcessContext.SaveChangesAsync();
        }
    }
}
