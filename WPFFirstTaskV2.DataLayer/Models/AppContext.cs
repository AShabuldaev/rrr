namespace WPFFirstTaskV2.DataLayer.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Mapping;
    public partial class AppContext : DbContext
    {
        public AppContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<ProcessModel> ProcessModels { get; set; }
        public virtual DbSet<TaskModel> TaskModels { get; set; }
        public virtual DbSet<UserModel> UserModels { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new ProcessMap());
            modelBuilder.Configurations.Add(new TaskMap());
        }
    }
}
