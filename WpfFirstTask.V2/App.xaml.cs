﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Globalization;

namespace WpfFirstTask.V2
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static List<CultureInfo> AppLanguages = new List<CultureInfo>();

        public static List<CultureInfo> Languages
        {
            get
            {
                return AppLanguages;
            }
        }

        public App()
        {
            InitializeComponent();
            App.LanguageChanged += App_LanguageChanged;
            AppLanguages.Clear();
            AppLanguages.Add(new CultureInfo("en-US"));
            AppLanguages.Add(new CultureInfo("ru-RU"));
            Lang = WpfFirstTask.V2.Properties.Settings.Default.DefaultLanguage;
        }

        public static CultureInfo Lang
        {
            get
            {
                return System.Threading.Thread.CurrentThread.CurrentUICulture;
            }
            set
            {
                if (value == null) throw new ArgumentNullException("value");

                if (value == System.Threading.Thread.CurrentThread.CurrentUICulture) return;
                
                System.Threading.Thread.CurrentThread.CurrentUICulture = value;
                
                ResourceDictionary dict = new ResourceDictionary();

                switch (value.Name)
                {
                    case "ru-RU":

                        dict.Source = new Uri(string.Format("Resources/lang.{0}.xaml", value.Name), UriKind.Relative);

                        break;

                    default:

                        dict.Source = new Uri("Resources/lang.xaml", UriKind.Relative);

                        break;
                }
                
                ResourceDictionary oldDict = Application.Current.Resources.MergedDictionaries
                    .Where(d => d.Source != null && d.Source.OriginalString.StartsWith("Resources/lang."))
                    .First();

                if (oldDict != null)
                {
                    int ind = Current.Resources.MergedDictionaries.IndexOf(oldDict);

                    Current.Resources.MergedDictionaries.Remove(oldDict);

                    Current.Resources.MergedDictionaries.Insert(ind, dict);
                }
                else
                {
                    Current.Resources.MergedDictionaries.Add(dict);
                }
                
                LanguageChanged(Current, new EventArgs());
            }
        }

        public static event EventHandler LanguageChanged;

        private void App_LanguageChanged(object sender, EventArgs e)
        {
            WpfFirstTask.V2.Properties.Settings.Default.DefaultLanguage = Lang;
            WpfFirstTask.V2.Properties.Settings.Default.Save();
        }
    }
}
