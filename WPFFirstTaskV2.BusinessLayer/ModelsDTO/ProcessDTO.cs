﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFFirstTaskV2.BusinessLayer.ModelsDTO
{
    /// <summary>
    /// промежуточная модель процесса
    /// </summary>
    public class ProcessDTO
    {
        
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatingDate { get; set; }
        public DateTime СompletionDate { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }
        public int UserId { get; set; }
    }
}
