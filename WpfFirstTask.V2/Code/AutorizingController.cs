﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.BusinessLayer.Services;
using WPFFirstTaskV2.BusinessLayer.User;
using WPFFirstTaskV2.BusinessLayer.ModelsDTO;
using System.Collections.ObjectModel;
using WpfFirstTask.V2.ViewModels;
using WpfFirstTask.V2.ProgramEventArgs;
namespace WpfFirstTask.V2.Code
{
    /// <summary>
    /// Обрабатывает запросы пользователя при авторизации
    /// </summary>
    public class AutorizingController
    {
        /// <summary>
        /// Производит операцию авторизации
        /// </summary>
        /// <param name="window">рабочее окно приложения</param>
        /// <returns>возвращает модель представления аккаунта
        /// полученную из GetAccountContent, в случае введения неверных данных
        /// или отстутствия аккаунта возбуждает исключение NotImpementedExeption
        /// </returns>
        public async Task<AccountViewModel> EnterAccount(MainWindow window)
        {
            Authorization authorization = new Authorization();
            Authorization += Authorizing;
            UserLoggined += AuthorizingOK;
            Authorization?.Invoke(window, new ProgramStateEventArgs(window.FindResource("Authorizing").ToString()));
            AccountDTO account = new AccountDTO();
            try
            {
                account = await authorization.GetAccount(window.LoginBoxAuthor.Text, window.PasswordBoxAuthor.Password);
            }
            catch (NotImplementedException)
            {
                
                System.Windows.MessageBox.Show(window.FindResource("WrongPasswOrLog").ToString());

                window.StateTextBlock.Text = string.Empty;
                throw new NotImplementedException();

            }
            catch
            {
                
                System.Windows.MessageBox.Show(window.FindResource("UnExpectedErr").ToString());

                window.StateTextBlock.Text = string.Empty;
                throw new NotImplementedException();
            }

            window.LoginBoxAuthor.Text = string.Empty;
            window.PasswordBoxAuthor.Password = string.Empty;

            AccountViewModel a = GetAccountContent(account);
            UserLoggined?.Invoke(window, new ProgramStateEventArgs(window.FindResource("UAuthorized").ToString()));
            return a;
        }
        /// <summary>
        /// получает из бд аккаунт по его логину и паролю
        /// добавляет в аккаунт его процессы и задачи
        /// </summary>
        /// <param name="accountDTO">с помощью этого параметра происходит работа с Services
        /// из BBL
        /// </param>
        /// <returns> возвращает модель представления аккаунта</returns>
        private AccountViewModel GetAccountContent(AccountDTO accountDTO)
        {
            //инициализирует accountView
            AccountViewModel accountView = new AccountViewModel()
            {
                Id = accountDTO.Id,
                Name = accountDTO.AccountName
            };
            try
            {
                //получение процессов аккаунта
                foreach (var proc in accountDTO.AccountProcesses)
                {
                    var TaskViewModels = new ObservableCollection<TaskViewModel>();
                    //получение задач каждого процесса в аккаунте
                    foreach (var c in proc.Tasks)
                    {
                        TaskViewModels.Add(new TaskViewModel(c));
                    }

                    ProcessViewModel procVM = new ProcessViewModel(proc);
                    procVM.Tasks = TaskViewModels;

                    accountView.AccountProcesses.Add(procVM);
                }
            }
            catch
            {

            }
            
            return accountView;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">вызывающий объект(MainWindow)</param>
        /// <param name="e">аргументы (выводимое сообщение)</param>
        delegate void AutorizingStateHandler(object sender, ProgramStateEventArgs e);
        /// <summary>
        /// ивент при авторизации(выводит сообщение о том,что происходит авторизация)
        /// </summary>
        event AutorizingStateHandler Authorization;
        /// <summary>
        /// ивент при успешной авторизации
        /// </summary>
        event AutorizingStateHandler UserLoggined;
        
        /// <summary>
        /// Передает в строку состояния информацию о том,что происходит авторизация
        /// </summary>
        /// <param name="sender">вызывающий объект(MainWindow)</param>
        /// <param name="e"> аргументы (выводимое сообщение) </param>
        private void Authorizing(object sender, ProgramStateEventArgs e)
        {
            MainWindow window = sender as MainWindow;

            window.StateTextBlock.Text = e.Message;
        }
        /// <summary>
        /// Передает в строку состояния информацию о том,что была выполнена авторизация
        /// </summary>
        /// <param name="sender">вызывающий объект(MainWindow)</param>
        /// <param name="e">аргументы (выводимое сообщение) </param>
        private async void AuthorizingOK(object sender, ProgramStateEventArgs e)
        {
            MainWindow window = sender as MainWindow;

            window.StateTextBlock.Text = e.Message;
            await Task.Run(async () =>
            {
                await Task.Delay(window.DelayStateString);

            });

            window.StateTextBlock.Text = string.Empty;
        }
    }
}
