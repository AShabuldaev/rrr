﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.DataLayer.Models;
namespace WPFFirstTaskV2.DataLayer.Interfaces
{
    public interface IUserRepository
    {
        Task<UserModel> FindAsync(string login);
        void CreateAsync(UserModel user);
    }
}
