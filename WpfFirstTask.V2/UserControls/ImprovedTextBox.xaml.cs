﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfFirstTask.V2.UserControls
{
    /// <summary>
    /// Логика взаимодействия для ImprovedTextBox.xaml
    /// </summary>
    public partial class ImprovedTextBox : UserControl
    {
        public ImprovedTextBox()
        {
            InitializeComponent();
        }

        private void ClearClick(object sender, RoutedEventArgs e)
        {
            TxtB.Text = string.Empty;
        }

        public string Text
        {
            get
            {
                return TxtB.Text;
            }
            set
            {
                TxtB.Text = value;
            }
        }

        public static readonly DependencyProperty CurrentProperty =
            DependencyProperty.Register("Text", 
                typeof(string),
                typeof(ImprovedTextBox),
                new FrameworkPropertyMetadata("",new PropertyChangedCallback(TextChanged)) { BindsTwoWayByDefault = true }
                );
     
        private static void TextChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs args)
        {
            ImprovedTextBox improvedTextBox = (ImprovedTextBox)depObj;

            TextBox textBox = improvedTextBox.TxtB;

            textBox.Text = args.NewValue.ToString();
        }

       
    }
}
