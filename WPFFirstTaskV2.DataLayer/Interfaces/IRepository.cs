﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFFirstTaskV2.DataLayer.Interfaces
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// получение всех объектов типа Т
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();
        /// <summary>
        /// получение одного объекта по введённому Id
        /// </summary>
        /// <param name="id">Id нужного объекта</param>
        /// <returns></returns>
        Task<T> GetAsync(int id);
        /// <summary>
        /// создать объект Т
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task CreateAsync(T item);
        /// <summary>
        /// Обновить
        /// </summary>
        /// <param name="item">элемент который нужно обновить</param>
        /// <returns></returns>
        Task UpdateAsync(T item);
        /// <summary>
        /// удалить объект
        /// </summary>
        /// <param name="id">ключ удаляемого объекта</param>
        /// <returns></returns>
        Task DeleteAsync(int id);
        /// <summary>
        /// получение последнего элемента коллекции
        /// </summary>
        /// <returns></returns>
        Task<T> GetLastAsync();
    }
}
