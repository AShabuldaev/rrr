﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.BusinessLayer.Interfaces;
using WPFFirstTaskV2.BusinessLayer.ModelsDTO;
using WPFFirstTaskV2.DataLayer.Repositories;
using WPFFirstTaskV2.DataLayer.Models;
using AutoMapper;
namespace WPFFirstTaskV2.BusinessLayer.Services
{
    public class TasksServices : IClientService<TaskDTO>
    {
        /// <summary>
        /// предоставляет сервис для работы с задачами
        /// </summary>
        Repositories Database = new Repositories();

        public async Task Delete(int id)
        {
            await Database.Tasks.DeleteAsync(id);
        }

        public async Task Edit(TaskDTO item)
        {
            TaskModel model = new TaskModel()
            {
                Name = item.Name,
                Id = item.Id,
                CreatingDate = item.CreatingDate,
                СompletionDate = item.СompletionDate,
                ProcessId = item.ProcessId
            };
            try
            {
                await Database.Tasks.UpdateAsync(model);
            }
            catch { }

        }

        public IEnumerable<TaskDTO> GetAll()
        {
            var Map = new MapperConfiguration(conf => conf.CreateMap<TaskModel, TaskDTO>()).CreateMapper();

            return Map.Map<IEnumerable<TaskModel>, List<TaskDTO>>(Database.Tasks.GetAll());
        }



        public async Task Add(TaskDTO item)
        {
            TaskModel model = new TaskModel()
            {
                CreatingDate = item.CreatingDate,
                СompletionDate = item.СompletionDate,
                Name = item.Name,
                ProcessId = item.ProcessId
            };
            await Database.Tasks.CreateAsync(model);
        }
        /// <summary>
        /// получение последнего эл-та из таблицы задач
        /// </summary>
        /// <returns></returns>
        public async Task<TaskDTO> Last()
        {

            var last = await Database.Tasks.GetLastAsync();

            return new TaskDTO()
            {
                Id = last.Id,
                Name = last.Name,
                CreatingDate = last.CreatingDate,
                СompletionDate = last.СompletionDate,
                ProcessId = last.ProcessId
            };
        }
    }
}
