﻿using System;
using WPFFirstTaskV2.DataLayer.Models;
using WPFFirstTaskV2.DataLayer.Interfaces;

namespace WPFFirstTaskV2.DataLayer.Repositories
{
    public class Repositories : IUnitOfWork
    {
        /// <summary>
        /// Класс через который происходит работа с репозиториями
        /// </summary>

        private AppContext context;

        private ProcessRepository processRepository;

        private TaskRepository taskRepository;

        //устанавливает контекст данных при создании экземпляра UnitOfWork
        public Repositories()
        {
            context = new AppContext();
        }

        public IRepository<ProcessModel> Processes
        {
            get
            {
                if (processRepository == null)
                    processRepository = new ProcessRepository(context);
                return processRepository;
            }
        }

        public IRepository<TaskModel> Tasks
        {
            get
            {
                if (taskRepository == null)
                    taskRepository = new TaskRepository(context);
                return taskRepository;
            }
        }

        public async void Save()
        {
            await context.SaveChangesAsync();
        }

        private bool disposed = false;


        #region
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
