﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFFirstTaskV2.BusinessLayer.Interfaces
{

    public interface IClientService<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns>возвращает коллекцию всех элементов из таблицы</returns>
        IEnumerable<T> GetAll();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item">изменяемый объект</param>
        /// <returns></returns>
        Task Edit(T item);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"> ключ удаляемого объекта</param>
        /// <returns></returns>
        Task Delete(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item">добавляемый объект</param>
        /// <returns></returns>
        Task Add(T item);
    }
}
