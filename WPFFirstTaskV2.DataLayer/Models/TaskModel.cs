namespace WPFFirstTaskV2.DataLayer.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatingDate { get; set; }

        public DateTime СompletionDate { get; set; }

        public int ProcessId { get; set; }
    }
}
