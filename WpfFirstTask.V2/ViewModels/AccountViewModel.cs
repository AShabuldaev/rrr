﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
namespace WpfFirstTask.V2.ViewModels
{
    /// <summary>
    /// Модель представления аккаунта
    /// </summary>
    public class AccountViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ObservableCollection<ProcessViewModel> AccountProcesses { get; set; } = new ObservableCollection<ProcessViewModel>();

        public ProcessViewModel CurrentProcess { get; set; }
    }
}
