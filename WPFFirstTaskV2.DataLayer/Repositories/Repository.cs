﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.DataLayer.Models;
using WPFFirstTaskV2.DataLayer.Interfaces;
namespace WPFFirstTaskV2.DataLayer.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : class, new()
    {

        /// <summary>
        /// Класс для работы с данными
        /// </summary>
        public AppContext ProcessContext { get; set; }

        public Repository(AppContext procCont)
        {
            ProcessContext = procCont;
        }

        public abstract Task<T> GetLastAsync();

        public async Task<T> GetAsync(int id)
        {
            return await ProcessContext.Set<T>().FindAsync(id);
        }

        public async Task CreateAsync(T item)
        {
            ProcessContext.Set<T>().Add(item);

            await ProcessContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            T item = await ProcessContext.Set<T>().FindAsync(id);
            if (item != null)
            {
                ProcessContext.Set<T>().Remove(item);

                await ProcessContext.SaveChangesAsync();
            }
        }

        public IEnumerable<T> GetAll()
        {
            return ProcessContext.Set<T>();
        }

        public abstract Task UpdateAsync(T item);
    }
}
