﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfFirstTask.V2.ViewModels;
using WPFFirstTaskV2.BusinessLayer.Services;
using WPFFirstTaskV2.BusinessLayer.ModelsDTO;
namespace WpfFirstTask.V2.Code
{
    /// <summary>
    /// Обрабатывает запросы пользователя при работе с задачами
    /// </summary>
    public class TaskController
    {
        /// <summary>
        /// Обрабатывает запрос на создание новой задачи
        /// </summary>
        /// <param name="window">рабочее окно приложения</param>
        public async void AddNewTask(MainWindow window)
        {
            TasksServices tasksServices = new TasksServices();

            TaskDTO task = new TaskDTO()
            {
                Name = window.NameTxtBTaskCreatingTab.Text,
                CreatingDate =(DateTime) window.CreatingTaskDateCreatingTab.SelectedDate,
                СompletionDate = (DateTime) window.CompletionTaskDateCreatingTab.SelectedDate,
                ProcessId = window.CurrentAccount.CurrentProcess.Id
            };

            await tasksServices.Add(task);
            task = await tasksServices.Last();

            var taskView = new TaskViewModel(task);

            window.CurrentAccount.CurrentProcess.Tasks.Add(taskView);
            window.NameTxtBTaskCreatingTab.Text = string.Empty;
            window.CreatingTaskDateCreatingTab.SelectedDate = null;
            window.CompletionTaskDateCreatingTab.SelectedDate = null;
        }

        /// <summary>
        /// Обрабатывает запрос на удаление задачи
        /// </summary>
        /// <param name="processViewModel">процесс в котором удалить задачу</param>
        /// <param name="taskView">задача</param>
        public async void DeleteTask(ProcessViewModel processViewModel,TaskViewModel taskView)
        {
            TasksServices tasksServices = new TasksServices();

            processViewModel.Tasks.Remove(taskView);

            await tasksServices.Delete(taskView.Id);
        }

        /// <summary>
        /// Обрабатывает запрос на изменение задачи
        /// </summary>
        /// <param name="taskView">изменяемая задача</param>
        public async void Edit(TaskViewModel taskView)
        {
            TasksServices tasksServices = new TasksServices();

            await tasksServices.Edit(new TaskDTO()
            {
                Id = taskView.Id,
                CreatingDate = taskView.CreatingDate,
                Name = taskView.Name,
                СompletionDate = taskView.СompletionDate,
                ProcessId = taskView.ProcessId
            });
        }
    }
}
