﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
namespace WPFFirstTaskV2.DataLayer.Models.Mapping
{
    /// <summary>
    /// маппинг между моделью задачи и таблицей задач
    /// </summary>
    class TaskMap : EntityTypeConfiguration<TaskModel>
    {
        public TaskMap()
        {
            HasKey(t => t.Id);
            ToTable("TaskModels");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatingDate).HasColumnName("CreatingDate");
            Property(t => t.СompletionDate).HasColumnName("СompletionDate");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.ProcessId).HasColumnName("ProcessId");
        }
    }
}
