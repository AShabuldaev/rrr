﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace WPFFirstTaskV2.DataLayer.Models.Mapping
{
    /// <summary>
    /// маппинг между моделью пользователя и таблицей пользовател
    /// </summary>
    class UserMap : EntityTypeConfiguration<UserModel>
    {
        public UserMap()
        {
            HasKey(t => t.Id);
            ToTable("UserModels");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Login).HasColumnName("Login");
            Property(t => t.Password).HasColumnName("Password");
        }
    }
}
