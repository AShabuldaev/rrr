﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WPFFirstTaskV2.BusinessLayer.ModelsDTO;
using WPFFirstTaskV2.BusinessLayer.Services;
using System.Collections.ObjectModel;
namespace WpfFirstTask.V2.ViewModels
{
    public class TaskViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Описывает модель представления
        /// </summary>
        private TaskDTO taskDTO;

        public TaskViewModel(TaskDTO task)
        {
            taskDTO = task;
        }

        public int Id
        {
            get
            {
                return taskDTO.Id;
            }
            set
            {
                taskDTO.Id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        public int ProcessId
        {
            get
            {
                return taskDTO.ProcessId;
            }
            set
            {
                taskDTO.ProcessId = value;
                OnPropertyChanged(nameof(ProcessId));
            }
        }

        public string Name
        {
            get
            {
                return taskDTO.Name;
            }
            set
            {
                taskDTO.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public DateTime CreatingDate
        {
            get
            {
                return taskDTO.CreatingDate;
            }
            set
            {
                taskDTO.CreatingDate = value;
                OnPropertyChanged(nameof(CreatingDate));
            }
        }

        public DateTime СompletionDate
        {
            get
            {
                return taskDTO.СompletionDate;
            }
            set
            {
                taskDTO.СompletionDate = value;
                OnPropertyChanged(nameof(СompletionDate));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
