﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfFirstTask.V2.ProgramEventArgs
{
    /// <summary>
    /// Представляет собой класс аргументов для состояния задачи
    /// </summary>
    public class ProgramStateEventArgs
    {
        public string Message { get; set; }
        public ProgramStateEventArgs(string mes)
        {
            Message = mes;
        }
    }
}
