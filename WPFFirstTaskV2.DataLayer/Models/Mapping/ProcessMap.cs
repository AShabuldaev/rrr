﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace WPFFirstTaskV2.DataLayer.Models.Mapping
{
    /// <summary>
    /// маппинг между моделью процсса и таблицей процессов
    /// </summary>
    class ProcessMap : EntityTypeConfiguration<ProcessModel>
    {
        public ProcessMap()
        {
            HasKey(t => t.Id);
            ToTable("ProcessModels");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatingDate).HasColumnName("CreatingDate");
            Property(t => t.СompletionDate).HasColumnName("СompletionDate");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.UserId).HasColumnName("UserId");
        }
    }
}
