﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFFirstTaskV2.DataLayer.Interfaces
{
    /// <summary>
    /// инкапсулирует работу с репозиториями
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Models.ProcessModel> Processes { get; }
        IRepository<Models.TaskModel> Tasks { get; }
        void Save();
    }
}
