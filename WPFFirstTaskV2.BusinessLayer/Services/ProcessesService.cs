﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFFirstTaskV2.BusinessLayer.Interfaces;
using WPFFirstTaskV2.BusinessLayer.ModelsDTO;
using WPFFirstTaskV2.DataLayer.Repositories;
using WPFFirstTaskV2.DataLayer.Models;
using AutoMapper;

namespace WPFFirstTaskV2.BusinessLayer.Services
{
    /// <summary>
    /// предоставляет сервис для работы с процессами
    /// </summary>
    public class ProcessesService : IClientService<ProcessDTO>
    {
        public Repositories Database = new Repositories();

        public IEnumerable<ProcessDTO> GetAll()
        {
            var processes = Database.Processes.GetAll();
            var Map = new MapperConfiguration(conf => conf.CreateMap<ProcessModel, ProcessDTO>()).CreateMapper();

            return Map.Map<IEnumerable<ProcessModel>, List<ProcessDTO>>(processes);
        }

        public async Task Delete(int id)
        {
           await Database.Processes.DeleteAsync(id);
        }

        public async Task Edit(ProcessDTO item)
        {
            ProcessModel model = new ProcessModel()
            {
                Id = item.Id,
                CreatingDate = item.CreatingDate,
                СompletionDate = item.СompletionDate,
                Name = item.Name,
                UserId = item.UserId
            };
            try
            {
                await Database.Processes.UpdateAsync(model);
            }
            catch
            {

            }

        }

        public async Task Add(ProcessDTO item)
        {
            ProcessModel model = new ProcessModel()
            {
                Id = item.Id,
                CreatingDate = item.CreatingDate,
                СompletionDate = item.СompletionDate,
                Name = item.Name,
                UserId = item.UserId
            };
            await Database.Processes.CreateAsync(model);
        }

        private ICollection<TaskDTO> GetAny(int? procId)
        {
            var db = Database.Tasks.GetAll();
            db = db.Where(t => t.ProcessId == procId.Value);
            return db as ICollection<TaskDTO>;
        }
        /// <summary>
        /// удаляет задачи процесса
        /// </summary>
        /// <param name="processId">ключ процсса,для которого нужно удалить задачи</param>
        public void RemoveTasks(int processId)
        {
            var db = Database.Tasks.GetAll();
            db = db.Where(t => t.ProcessId == processId);
            foreach (var task in db)
                Database.Tasks.DeleteAsync(task.Id);
        }
        /// <summary>
        /// получение последнего эл-та из таблицы процессов
        /// </summary>
        /// <returns></returns>
        public async Task<ProcessDTO> LastAsync()
        {

            var last = await Database.Processes.GetLastAsync();

            return new ProcessDTO()
            {
                Id = last.Id,
                Name = last.Name,
                CreatingDate = last.CreatingDate,
                СompletionDate = last.СompletionDate
            };
        }
    }
}
